'use strict';

export default class Styck {
	constructor(options){
		this.data = {};
		Object.assign(this._options = {}, this._default(), options);
	}

	_default(){
		return {
			element: null
		}
	}

	_getCoords(el) {
		let box = el.getBoundingClientRect();

		return {
			top: box.top + pageYOffset,
			left: box.left + pageXOffset
		};
	}

	_setStyle(obj){
		Object.keys(obj).map((key)=>{
			if ( this.data.element.style[key] !== obj[key] ) this.data.element.style[key] = obj[key];
		});

	}

	_setPosition(){
		let data = {};

		data.toBottom = (window.pageYOffset - this.data.scrollTop) >= 0;
		data.scrolled = window.pageYOffset || document.documentElement.scrollTop;
		data.elHeight = this.data.element.offsetHeight;
		data.elTop = this._getCoords(this.data.element).top;
		data.elLeft = this._getCoords(this.data.element).left;
		data.winHeight = window.innerHeight;
		data.parentTop = this._getCoords(this.data.parent).top;
		data.parentLeft = this._getCoords(this.data.parent).left;
		data.parentHeight = this.data.parent.offsetHeight;


		if ( data.scrolled < data.parentTop ){
			this._setStyle({
				position: '',
				left: '',
				top: '',
				bottom: ''
			});
		} else if ( data.scrolled + data.winHeight > data.parentHeight + data.parentTop ){
			this._setStyle({
				position: 'absolute',
				left: '',
				top: data.parentHeight - data.elHeight + 'px',
				bottom: ''
			});
		} else {
			if ( data.elHeight <= data.winHeight ){
				this._setStyle({
					position: 'fixed',
					left: data.parentLeft + 'px',
					top: 0,
					bottom: ''
				});
			} else {
				this.data.parent.style.minHeight = data.elHeight + 'px';
				if ( data.toBottom && data.scrolled + data.winHeight >= data.elHeight + data.elTop) {
					this._setStyle({
						position: 'fixed',
						top: '',
						left: data.parentLeft + 'px',
						bottom: 0
					});
				} else if ( !data.toBottom && data.scrolled <= data.elTop ){
					this._setStyle({
						position: 'fixed',
						top: 0,
						left: data.parentLeft + 'px',
						bottom: ''
					});
				} else {
					this._setStyle({
						position: 'absolute',
						top: data.elTop - data.parentTop + 'px',
						left: '',
						bottom: ''
					});
				}

			}
		}

	}

	init(){
		this.data.element = document.querySelector(this._options.element);
		this.data.parent = this.data.element.parentNode;
		this.data.scrollTop = window.pageYOffset;
		this._setPosition();
		window.onscroll = ()=>{
			this._setPosition();
			this.data.scrollTop = window.pageYOffset;
		};
		window.onresize = ()=>this._setPosition();
	}

}