'use strict';

export default class Calendar {

	constructor(options) {
		this._polyfill();

		Object.assign(this._options = {}, this._default(), options);
		this._init();
	}

	_polyfill() {

		//assign
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}
	}

	_default(){
		return {
			events: {},
			offsetWeek: 1,
			today: {
				day: null,
				week: null,
				month: null,
				year: null
			},
			current: {
				month: null,
				year: null
			},
			month: {
				short: ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'],
				full: ['январь','февраль','март','апрель','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь'],
				decline: ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря']
			},
			week: {
				short: ['вс','пн','вт','ср','чт','пт','сб'],
				full: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота']
			},
			onInit: ()=>{},
			onChange: ()=>{}
		}
	}



	_isLeapYear(year){
		return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
	}

	_amountDay(month, year){
		let d = [31, (this._isLeapYear(year || this._options.current.year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		return d[(month >= 0 ? month : this._options.current.month)];
	}

	updateDates(arr){
		this._options.dates = arr;
	}

	_setToday(){
		let now = new Date();
		this._options.today.day = now.getDate();
		this._options.today.month = now.getMonth();
		this._options.today.year = now.getFullYear();
		this._options.today.week = now.getDay();
	}

	_getMonth(){
		let calendar = {
				weeks: {
					short: [],
					full: []
				},
				month: {
					short: this._options.month.short[this._options.current.month],
					full: this._options.month.full[this._options.current.month],
					decline: this._options.month.decline[this._options.current.month]
				},
				days: [],
				year: this._options.current.year
			},
			amountDay = this._amountDay(),
			firstWeekMonth = new Date(this._options.current.year, this._options.current.month, 1).getDay()
		;

		for (let i = 0; i < this._options.week.short.length; i++){
			calendar.weeks.short.push(this._options.week.short[(i + this._options.offsetWeek)%7]);
			calendar.weeks.full.push(this._options.week.full[(i + this._options.offsetWeek)%7]);
		}

		for (let d = 0; d < amountDay; d++ ){

			let isToday = this._options.current.year === this._options.today.year && this._options.current.month === this._options.today.month && d + 1 === this._options.today.day ,
				isPast = this._options.current.year < this._options.today.year || (this._options.current.year === this._options.today.year && this._options.current.month < this._options.today.month) || (this._options.current.year === this._options.today.year && this._options.current.month === this._options.today.month && d + 1 < this._options.today.day ),
				day = {
					num: d + 1,
					date: this.toCustomDate(d + 1, this._options.current.month + 1, this._options.current.year),
					week: {
						short: this._options.week.short[(firstWeekMonth + d) % 7],
						full: this._options.week.full[(firstWeekMonth + d) % 7]
					},
					month: {
						short: this._options.month.short[this._options.current.month],
						full: this._options.month.full[this._options.current.month],
						decline: this._options.month.decline[this._options.current.month]
					},
					year: this._options.current.year
				};


			if ( isToday ) {
				day.today = !0;
			} else if ( isPast ){
				day.past = !0;
			} else {
				day.future = !0;
			}

			if ( this._options.events[this._options.current.year] && this._options.events[this._options.current.year][this._options.current.month + 1] && this._options.events[this._options.current.year][this._options.current.month + 1][d + 1] && this._options.events[this._options.current.year][this._options.current.month + 1][d + 1].length){
				day.events = this._options.events[this._options.current.year][this._options.current.month + 1][d + 1];
			}

			calendar.days.push(day)
		}


		let pm = 0,
			pmMonth = this._options.current.month - 1 < 0 ? 11 : this._options.current.month - 1,
			pmYear = pmMonth === 11 ? this._options.current.year - 1 : this._options.current.year,
			pmAmountDay = this._amountDay(pmMonth, pmYear);

		for (pm; pm < (firstWeekMonth + 7 - this._options.offsetWeek)%7; pm++){
			let pmWeek = firstWeekMonth - 1 - (pm%7) < 0 ? 6 : firstWeekMonth - 1 - (pm%7),
				day = {
					num: pmAmountDay - pm,
					week: {
						short: this._options.week.short[pmWeek],
						full: this._options.week.full[pmWeek]
					},
					date: this.toCustomDate(pmAmountDay - pm, pmMonth + 1, pmYear),
					prevMonth: !0,
					month: {
						short: this._options.month.short[pmMonth],
						full: this._options.month.full[pmMonth],
						decline: this._options.month.decline[pmMonth]
					},
					year: this._options.current.year
				};

			calendar.days.unshift(day);
		}

		for (let nm = 0; nm < (7 - (amountDay + firstWeekMonth)%7 + this._options.offsetWeek)%7; nm++){
			let nmWeek = (firstWeekMonth + amountDay + nm) % 7,
				day = {
					num: nm + 1,
					week: {
						short: this._options.week.short[nmWeek],
						full: this._options.week.full[nmWeek]
					},
					date: this.toCustomDate(nm + 1, this._options.current.month%12 + 2, this._options.current.year),
					nextMonth: !0,
					month: {
						short: this._options.month.short[this._options.current.month%12 + 1],
						full: this._options.month.full[this._options.current.month%12 + 1],
						decline: this._options.month.decline[this._options.current.month%12 + 1]
					},
					year: this._options.current.year
				};

			calendar.days.push(day);
		}

		this._options.onChange(calendar);

		//console.info(this._options.events, this._options.current.year, this._options.current.month);

		return calendar;
	}

	toCustomDate(d,m,y){
		return `${(''+d).length < 2 ? '0' + d : d}-${(''+m).length < 2 ? '0' + m : m}-${y}`
	}

	setDate(d){
		let date = d.split("-");

		this._options.today.day = date[0] - 1;
		this._options.current.month = date[1] - 1;
		this._options.current.year = date[2];

		this._getMonth();
	}

	prevMonth(){
		this._options.current.month = this._options.current.month - 1 < 0 ? 11 : this._options.current.month - 1;
		this._options.current.year = this._options.current.month === 11 ? this._options.current.year - 1 : this._options.current.year;
		this._getMonth();
	}

	nextMonth(){
		this._options.current.month = this._options.current.month + 1 > 11 ? 0 : this._options.current.month + 1;
		this._options.current.year = this._options.current.month === 0 ? this._options.current.year + 1 : this._options.current.year;

		this._getMonth();
	}

	prevYear(){
		this._options.current.year--;
		this._getMonth();
	}

	nextYear(){
		this._options.current.year++;
		this._getMonth();
	}

	addEvents(arr){
		let events = {}, i = 0;
		for (i; i<arr.length; i++){
			let date = arr[i].event_date.split('-'),
				d = date[2].replace(/^0/ig, ''),
				m = date[1].replace(/^0/ig, ''),
				y = date[0].replace(/^0/ig, '')
			;

			if ( !events[y] ) events[y] = {};
			if ( !events[y][m] ) events[y][m] = {};
			if ( !events[y][m][d] ) events[y][m][d] = [];

			events[y][m][d].push(arr[i]);
		}
		this._options.events = events;
		this._getMonth();
	}

	getMonth(month = this._options.current.month, year = this._options.current.year){
		this._options.current.month = month;
		this._options.current.year = year;
		this._getMonth();
	}

	todayMonth(){
		this.getMonth(this._options.today.month, this._options.today.year);
	}

	_init(){
		this._setToday();
		this._options.onInit();
	}

}
