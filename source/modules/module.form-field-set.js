'use strict';

export default class FormFieldSet {
	constructor(arr){
		this.fields = arr;
	}

	setValue(name, val){
		for (let i = 0; i < this.fields.length; i++){
			if (this.fields[i].field.name === name){
				if ( this.fields[i].field.type === 'multiple'){
					this.fields[i].value = decodeURI(val).split(',');
				} else {
					this.fields[i].value = decodeURI(val);
				}
				break;
			}
		}

	}

}