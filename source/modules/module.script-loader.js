'use strict';

let collection = {};

export default {
	
	load(url, callback){

		if (!url) return false;
		if (!collection[url]) {
			collection[url] = {};
			collection[url].status = null;
			collection[url].callbacks = []
		}

		let i = 0, j = 0, script = null, prior;

		for (i; i < document.getElementsByTagName('script').length; i++){

			if ( collection[url].status === 'complete' ){
				callback();
				return;
			} else if ( collection[url].status === 'loading' ){
				collection[url].callbacks.push(callback)
			}
		}

		script = document.createElement('script');
		prior = document.getElementsByTagName('script')[0];
		script.async = 1;
		script.onload = script.onreadystatechange = function( _, isAbort ) {
			if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState) ) {
				script.onload = script.onreadystatechange = null;
				script = undefined;

				if(!isAbort) {
					collection[url].status = 'complete';
					if ( collection[url].callbacks.length ){
						for (j; j < collection[url].callbacks.length; j++){
							collection[url].callbacks[j]();
							collection[url].callbacks.splice(j, 1);
						}
					}
					if(callback) callback(true);
				}
			}
		};
		script.src = url;
		prior.parentNode.insertBefore(script, prior);

	}
}