'use strict';

import decline from 'petrovich'
import _ from 'lodash'

export default (info, v, m, d)=>{
	// console.info('мв1', m);

	let options = Object.assign({_local:{}}, ...m),
		month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
		eventDate = d ? new Date(`${d.split('-')[1]}-${d.split('-')[0]}-${d.split('-')[2]}`) : new Date(),
		date = {},
		vars = typeof v === 'string' ? JSON.parse(v) : v,
		obj = {},
		declineLeaderPosition = ()=>{
			let spl = options.entity_director.position.split(' '), jn = [];
			for (let i = 0; i < spl.length; i++){
				jn[i] = decline.male.last.genitive(spl[i]);
			}
			return jn.join(' ');
		},
		data = {}
	;

	// console.info(options);

	vars = _.map(vars, i=>i.trim());

	date.day = eventDate.getDate();
	date.month = eventDate.getMonth();
	date.year = eventDate.getFullYear();
	date.day_str = eventDate.getDate().toString() > 1 ? eventDate.getDate().toString() : '0' + eventDate.getDate();
	date.month_str = (eventDate.getMonth() + 1).toString() > 1 ? (eventDate.getMonth() + 1).toString() : '0' + (eventDate.getMonth() + 1);
	date.year_str = eventDate.getFullYear().toString();


	data = {
		// Даты
		crm_date_D: 		date.month_str,		// день месяца 01
		crm_date_Mn: 		date.day,		// месяц числом
		crm_date_Ms: 		month[date.month],		// месяц текстом
		crm_date_Y: 		date.year,		// год 2000
		crm_date_DMYn: 		`${date.day_str}.${date.month_str}.${date.year}`,	// дата числовая  01.01.2000
		crm_date_DMYs: 		`${date.day_str} ${month[date.month]} ${date.year} г.`,	// дата тексто-числовая  01 января 2000 г.
		crm_date_insert: 	`«${date.day_str}» ${month[date.month]} ${date.year} г.`,

		// Компания клиента

		crm_client_id: 				options.id,	// ID клиента в системе CRM

		crm_client_legal: 			info.getLegal(options.entity_legal).fullName,	// Форма организационной структуры
		crm_client_legal_short:		info.getLegal(options.entity_legal).shortName,	// Форма организационной структуры
		crm_client_name: 			options.entity_fullname,				// Полное название компании
		crm_client_name_short: 		options.entity_shortname || options.entity_fullname,	// Сокращенное название компании (если не указано, назнаяается полное)
		crm_client_address_legal: 	options.entity_legal_address,			// Юридический адрес компании клиента
		crm_client_address_fact: 	options.entity_actual_address,		// Фактический адрес компании клиента
		crm_client_phone: 			options.entity_phone,					// Телефон адрес компании клиента
		crm_client_email: 			options.entity_email,					// Email адрес компании клиента

		crm_client_stuff_lead_post:			options.entity_director.position,			// Должность руководителя
		crm_client_stuff_lead: 				`${options.entity_director.second_name} ${options.entity_director.first_name} ${options.entity_director.third_name}`,			// Фамилия Имя Отчество руководитедя
		crm_client_stuff_lead_short: 		`${options.entity_director.second_name} ${options.entity_director.first_name.slice(0,1)}. ${options.entity_director.third_name.slice(0,1)}.`,			// Фамилия И. О. руководитедя
		crm_client_stuff_lead_firstname:	options.entity_director.first_name,			// Имя руководитедя
		crm_client_stuff_lead_surname: 		options.entity_director.second_name,		// Фамилия И. О. руководитедя
		crm_client_stuff_lead_lastname: 	options.entity_director.third_name,			// Фамилия И. О. руководитедя

		crm_client_stuff_book: 				options.entity_accountant ? `${options.entity_accountant.second_name} ${options.entity_accountant.first_name} ${options.entity_accountant.third_name}` : '',							// Фамилия Имя Отчество бухгалтера
		crm_client_stuff_book_short:		options.entity_accountant ? `${options.entity_accountant.second_name} ${options.entity_accountant.first_name.slice(0,1)}. ${options.entity_accountant.third_name.slice(0,1)}.` : '',	// Фамилия И. О. бухгалтера
		crm_client_stuff_book_firstname:	options.entity_accountant ? options.entity_accountant.first_name : '',			// Имя руководитедя
		crm_client_stuff_book_surname: 		options.entity_accountant ? `${options.entity_director.second_name} ${options.entity_director.first_name} ${options.entity_director.third_name}` : '',			// Фамилия И. О. руководитедя
		crm_client_stuff_book_lastname: 	options.entity_accountant ? `${options.entity_director.second_name} ${options.entity_director.first_name} ${options.entity_director.third_name}` : '',			// Фамилия И. О. руководитедя

		// Реквизиты компании
		crm_client_bank_name: 		options.bank_name,		// Наименование банка клиента
		crm_client_bank_address: 	options.bank_placement,	// Адрес банка клиента
		crm_client_bank_bik: 		options.bank_bik,			// БИК банка клиента
		crm_client_bank_cc: 		options.bank_ccheck,		// Корреспандентский счет банка клиента
		crm_client_bank_rc: 		options.bank_rcheck,		// Расчетный счет клиента
		crm_client_inn: 			options.entity_inn,		// ИНН клиента
		crm_client_kpp: 			options.entity_kpp,		// КПП клиента
		crm_client_okpo: 			options.entity_okpo,		// ОКПО клиента
		crm_client_ogrn: 			options.entity_ogrn,		// ОГРН клиента

		crm_client_nds: 			options.entity_nds,		//:"0",

		crm_insert_client_leader: 	//:
			`в лице ${declineLeaderPosition().toLowerCase()} ${decline[options.entity_director.gender].last.genitive(options.entity_director.second_name)} ${decline[options.entity_director.gender].first.genitive(options.entity_director.first_name)} ${decline[options.entity_director.gender].middle.genitive(options.entity_director.third_name)} ${options.entity_director.gender === 'female' ? 'действующей' : 'действующего'}`
		,

		crm_insert_requisites: 	// шаблон
		`Юр. Адрес: ${options.entity_legal_address}` +
		`,<w:br/>ИНН/КПП: ${options.entity_inn}/${options.entity_kpp}` +
		(options.entity_ogrn ? `,<w:br/>ОГРН: ${options.entity_ogrn}` : ``) +
		`,<w:br/>р/c. № ${options.bank_rcheck} в ${options.bank_name}` +
		`,<w:br/>к/с ${options.bank_ccheck}` +
		`,<w:br/>БИК ${options.bank_bik}` +
		(options.entity_okpo ? `,<w:br/>ОКПО ${options.entity_okpo}` : ``) +
		(options.entity_phone ? `,<w:br/>тел.: ${options.entity_phone}` : ``) +
		(options.entity_email ? `,<w:br/>email: ${options.entity_email}`  : ``)
		,

	};

	for (let i = 0; i < vars.length; i++){
		obj[vars[i]] = data[vars[i]] || options._local[vars[i]];
	}

	// console.info('мв2', obj);
	return obj;

}
