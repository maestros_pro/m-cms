'use strict';


String.prototype.discharge = function(mark) {
	let x = this.split('.'),x1 = x[0],x2 = x.length > 1 ? '.' + x[1] : '',rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) { x1 = x1.replace(rgx, '$1' + (mark ? mark : ' ') + '$2'); }
	return x1 + x2;
};

Number.prototype.discharge = function(mark) {
	let val = this + '',x = val.split('.'),x1 = x[0],x2 = x.length > 1 ? '.' + x[1] : '',rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) { x1 = x1.replace(rgx, '$1' + (mark ? mark : ' ') + '$2'); }
	return x1 + x2;
};

String.prototype.declension = function() {
	let cases = [2, 0, 1, 1, 1, 2],num = Number(this);
	if ( isNaN(num) ){ console.error('declension must be a Number');}
	else { return arguments[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]]; }
};

Number.prototype.declension = function() {
	let cases = [2, 0, 1, 1, 1, 2],num = Number(this);
	return arguments[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
};

String.prototype.substrLengh = function(limit, postfix) {
	postfix = postfix || '...';
	let text = this.trim();
	if( text.length <= limit) return text;
	text = text.slice( 0, limit);
	let lastSpace = text.lastIndexOf(' ');
	if( lastSpace > 0) text = text.substr(0, lastSpace);
	return text + postfix;
};

Number.prototype.substrLengh = function(limit, postfix) {
	postfix = postfix|| '...';
	let text = this.toString().trim();
	if( text.length <= limit) return text;
	text = text.slice( 0, limit);
	let lastSpace = text.lastIndexOf(' ');
	if( lastSpace > 0) text = text.substr(0, lastSpace);
	return text + postfix;
};