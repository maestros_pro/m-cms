let prodUrl = '/request';

export default {
		authCheck: {
			method: 'authCheck',
			customData: {},
			url: DEVELOPMENT ? '/data/authuser.json' : prodUrl
		},
		logIn: {
			method: 'login',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		logOut:  {
			method: 'logout',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		setAuth:  {
			method: 'setAuth', 
			customData: {
				id: '',
				password: ''
			},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		unsetAuth:  {
			method: 'unsetAuth',
			customData: {
				id: ''
			},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		setInfoAuth:  {
			method: 'setInfoAuth',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},


		/**
		 * Company
		 */

		addCompany: {
			method: 'addCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/addCompany.json' : prodUrl
		},
		updateCompany: {
			method: 'updateCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/addCompany.json' : prodUrl
		},
		getCompanyItem: {
			method: 'getCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/counteragentitem.json' : prodUrl
		},
		getCompanyList: {
			method: 'getCompanyList',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/counteragentlist.json' : prodUrl
		},
		searchCompany: {
			method: 'searchCompany',
			customData: {
				count: 10,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/counteragentlist.json' : prodUrl
		},
		removeCompany: {
			method: 'removeCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getCompanyUsers: {
			method: 'getCompanyUsers',
			customData: {},
			url: DEVELOPMENT ? '/data/usercompany.json' : prodUrl
		},


		/**
		 * Objects
		 */

		getObjectList: {
			method: 'getObjectList',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getObjectList.json' : prodUrl
		},
		getObjectByCompany: {
			method: 'getObjectByCompany',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getObjectList.json' : prodUrl
		},
		getObject: {
			method: 'getObject',
			customData: {},
			url: DEVELOPMENT ? '/data/getObjectItem.json' : prodUrl
		},
		addObject: {
			method: 'addObject',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		updateObject: {
			method: 'updateObject',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		removeObject: {
			method: 'removeObject',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},


		/**
		 * Users
		 */

		addUser: {
			method: 'addUser',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getUserItem: {
			method: 'getUser',
			customData: {},
			url: DEVELOPMENT ? '/data/useritem.json' : prodUrl
		},
		updateUser: {
			method: 'updateUser',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		updateUserOwner: {
			method: 'updateUserOwner',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getUserList: {
			method: 'getUserList',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/userlist.json' : prodUrl
		},
		searchUser: {
			method: 'searchUser',
			customData: {
				count: 10,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/userlist.json' : prodUrl
		},
		removeUser: {
			method: 'removeUser',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},

		/**
		 * Events
		 */

		addEvent: {
			method: 'addEvent',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		confirmEvent: {
			method: 'confirmEvent',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		updateEvent: {
			method: 'updateEvent',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getEvent: {
			method: 'getEvent',
			customData: {},
			url: DEVELOPMENT ? '/data/eventitem.json' : prodUrl
		},
		getEventList: {
			method: 'getEventList',
			customData: {
				from: new Date().getFullYear() + '-01-01',
				to: new Date().getFullYear() + '-12-31'
			},
			url: DEVELOPMENT ? '/data/eventlist_.json' : prodUrl
		},
		getEventByUser: {
			method: 'getEventByUser',
			customData: {
				from: new Date().getFullYear() + '-01-01',
				to: new Date().getFullYear() + '-12-31'
			},
			url: DEVELOPMENT ? '/data/eventlist_.json' : prodUrl
		},
		getEventByCompany: {
			method: 'getEventByCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getEventByObject: {
			method: 'getEventByObject',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		removeEvent: {
			method: 'removeEvent',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getOverdueEventByUser: {
			method: 'getOverdueEventByUser',
			customData: {},
			url: DEVELOPMENT ? '/data/overdueEvents.json' : prodUrl
		},

		// templates

		addTemplate: {
			method: 'addTemplate',
			customData: {
				/**
				 * file
				 * description
				 * variables: [{name: name, description: description},{},{},...]
				 */
			},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		updateTemplate: {
			method: 'updateTemplate',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getTemplateItem: {
			method: 'getTemplate',
			customData: {},
			url: DEVELOPMENT ? '/data/getTemplateItem.json' : prodUrl
		},
		getTemplateList: {
			method: 'getTemplateList',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getTemplateList.json' : prodUrl
		},
		searchTemplate: {
			method: 'searchTemplate',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getTemplateList.json' : prodUrl
		},
		removeTemplate: {
			method: 'removeTemplate',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},

		// documents

		addDocument: {
			method: 'addDocument',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		updateDocument: {
			method: 'updateDocument',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		},
		getDocument: {
			method: 'getDocument',
			customData: {},
			url: DEVELOPMENT ? '/data/getDocumentItem.json' : prodUrl
		},
		getDocumentList: {
			method: 'getDocumentList',
			customData: {
				count: 0,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getDocumentList.json' : prodUrl
		},
		getDocumentFile: {
			method: 'getDocumentFile',
			customData: {
				count: 10,
				offset: 0
			},
			url: DEVELOPMENT ? '/data/getFile.json' : prodUrl
		},
		removeDocument: {
			method: 'removeCompany',
			customData: {},
			url: DEVELOPMENT ? '/data/login.json' : prodUrl
		}

	};