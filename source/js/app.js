import style from '../css/style.scss';

import Vue from 'vue';
import router from '../js/router.js'
import store from '../js/vuex.js'

import '../modules/module.stroke.js'

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard);

import Api from '../js/api'
Vue.use(Api);

import Info from '../js/info'
Vue.use(Info);

import layoutIndex from '../view/layout/layout.index.vue';

new Vue({
	el: '#app',
	filters: {},
	router,
	store,
	render: h => h(layoutIndex)
});
