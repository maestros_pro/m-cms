import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {

		auth: {},

		overdueEvents: [],

		dateEvent: false,

		loading: false,

		popup: {
			name: null,
			data: null
		},

		pageUpdate: null,

		pageData: null,

		form: {
			data: {},
			extra: {}
		}
	},

	mutations: {

		setOverdueEvents(s, b){
			s.overdueEvents = b;
		},

		setAuth(s, b){
			s.auth = b;
		},

		setDate (state, date) {
			state.dateEvent = date;
		},
		clearDate (state) {
			state.dateEvent = null;
		},
		startLoading (state){
			state.loading = true;
		},
		endLoading (state){
			state.loading = false;
		},

		// popups
		openPopup (state, options) {
			if ( typeof options === 'string') {
				state.popup.name = options;
			} else {
				state.popup.name = options.name;
				state.popup.data = options.data;
			}
			if (name !== null) document.body.classList.add('is-popup');
		},
		closePopup (state) {
			state.popup.name = null;
			state.popup.data = null;
			document.body.classList.remove('is-popup');
		},
		// page
		setPageData(state, data){
			state.pageData = data;
		},
		clearPageData(state){
			state.pageData = null;
		},
		// form
		setForm(state, data){
			if (data.data) state.form.data = data.data;
			if (data.extra) state.form.extra = data.extra;
		},
		clearForm(state){
			state.form.data = {};
			state.form.extra = {};
		},
		setFormData(state, data){
			for (let key in data) {
				if( data.hasOwnProperty( key ) ) {
					state.form.data[key] = data[key];
				}
			}
		},
		clearFormData(state, key){
			delete state.form.data[key];
		},

		pageUpdate(state){
			if ( state.pageUpdate && state.pageUpdate.update ) state.pageUpdate.update();
		}
	},


	getters: {

		getDate: state => state.dateEvent,
		isLoading: state => state.loading,

		getPopupName: state => state.popup.name,
		getPopupData: state => state.popup.data,

		getForm: state => state.form,
		getPageData: state => state.pageData,
	}
});



