import Vue from 'vue'
import Router from 'vue-router'


import Home from '../view/home.vue';
import Objects from '../view/Objects.vue';
import ObjectsItem from '../view/object.item.vue';
import Company from '../view/company.vue';
import CompanyItem from '../view/company.item.vue';
import Staff from '../view/staff.vue';
import StaffItem from '../view/staff.item.vue';
import Diary from '../view/diary.vue';
import Event from '../view/event.item.vue';
import Templates from '../view/templates.vue';
import TemplateItem from '../view/template.item.vue';
import Documents from '../view/documents.vue';
import DocumentItem from '../view/document.item.vue';
import Settings from '../view/settings.vue';
import Info from '../view/info.vue';
import Error from '../view/error.vue';

Vue.use(Router);

const routes = [
	{
		path: '/',
		component: Home,
		meta: {
			title: 'Home'
		}
	},
	{
		path: '/company',
		component: Company,
		meta: {
			title: 'Контрагенты'
		}
	},
	{
		path: '/company/:id',
		component: CompanyItem,
		name: 'company',
		meta: {
			title: 'Контрагент'
		}
	},
	{
		path: '/objects',
		component: Objects,
		meta: {
			title: 'Объекты'
		}
	},
	{
		path: '/object/:id',
		component: ObjectsItem,
		name: 'object',
		meta: {
			title: 'Объект'
		}
	},
	{
		path: '/staff',
		component: Staff,
		meta: {
			title: 'Персонал'
		}
	},
	{
		path: '/staff/:id',
		component: StaffItem,
		name: 'user',
		meta: {
			title: 'Персонал'
		}
	},
	{
		path: '/diary',
		component: Diary,
		meta: {
			title: 'Календарь'
		}
	},
	{
		path: '/event/:id',
		component: Event,
		name: 'event',
		meta: {
			title: 'Событие'
		}
	},
	{
		path: '/templates',
		component: Templates,
		meta: {
			title: 'Шаблоны документов'
		}
	},
	{
		path: '/template/:id',
		name: 'template',
		component: TemplateItem,
		meta: {
			title: 'Шаблон'
		}
	},
	{
		path: '/documents',
		component: Documents,
		meta: {
			title: 'Документы'
		}
	},
	{
		path: '/document/:id',
		component: DocumentItem,
		name: 'document',
		meta: {
			title: 'Документ'
		}
	},
	{
		path: '/settings',
		component: Settings,
		name: 'settings',
		meta: {
			title: 'Настройки'
		}
	},
	{
		path: '/info',
		component: Info,
		name: 'info',
		meta: {
			title: 'Информация'
		}
	},
	{
		path: '*',
		component: Error,
		meta: {
			title: 'Страница не существует'
		}
	}
];

const router = new Router({
	mode: 'history',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});


router.afterEach((to, from) => {
	document.title = to.meta.title;
});

export default router