'use strict';

import _ from 'lodash'

let storage = {
	events: [],
	legals: [],
	doctypes: [],
};

let info = {

	setInfo(obj){
		if ( obj ) {
			if ( typeof obj === 'string') {
				storage = JSON.parse(obj);
			} else {
				storage = obj;
			}
		}
	},

	getInfo(){
		return storage;
	},

	getEvent(val){
		if (val) return _.find(storage.events, {value: val.toString()});
		return storage.events;
	},

	getDocType(val){
		if (val) return _.find(storage.doctypes, {value: val.toString()});
		return storage.doctypes;
	},

	getLegal(val){
		if (val) return _.find(storage.legals, {value: val.toString()});
		return storage.legals;
	}
};

export default {
	install: (Vue, options)=>{
		Vue.prototype.$info = info;
	}
};
