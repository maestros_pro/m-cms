const DEVELOPMENT = process.env.NODE_ENV === 'development';
const PRODUCTION = process.env.NODE_ENV === 'production';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminWebpackPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
// const GoogleFontsPlugin = require('google-fonts-webpack-plugin');

const PATHS = {
	source: path.resolve(__dirname, 'source'),
	dist: path.resolve(__dirname, 'public'),
};

const config = {
	context: PATHS.source,

	entry: {
		script: './js/app.js'
	},

	output: {
		path: PATHS.dist,
		filename: 'js/[name].js'
	},

	devtool: 'source-map',

	devServer: {
		// publicPath: '/public',
		hot: true,
		inline: true,
		open: true,
		contentBase: "./public",
		historyApiFallback: {
			index: '/index.html',
		}
	},

	stats: {
		colors: true,
		modules: true,
		reasons: true,
		errorDetails: true,
	},

	plugins: [
		new webpack.DefinePlugin({
			'DEVELOPMENT': JSON.stringify(DEVELOPMENT),
			'process.env': {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV)
			}
		})
		,new webpack.ProvidePlugin({
			$: "jquery/dist/jquery.min.js",
			jQuery: "jquery/dist/jquery.min.js",
			"window.jQuery": "jquery/dist/jquery.min.js"
		})
		,new HtmlWebpackPlugin({
			template: PATHS.source + '/index.pug',
			inject: 'body'
		})
		,new FriendlyErrorsWebpackPlugin()
		,new webpack.NoEmitOnErrorsPlugin()
		,new webpack.optimize.OccurrenceOrderPlugin()
		,new webpack.HotModuleReplacementPlugin()
		/*,new GoogleFontsPlugin({
			fonts: [
				{
					family: "Open Sans",
					variants: ['400'],
					subsets: ['cyrillic']
				},
				{
					family: 'Vollkorn',
					variants: ['400'],
					subsets: ['cyrillic']
				}
			],
			filename: 'css/fonts.css'
		})*/
		,new ExtractTextPlugin({
			filename: 'css/style.css',
			// disable: process.env.NODE_ENV === "development"
		})
		,new CopyWebpackPlugin([
			{from: 'fonts/',to: 'fonts/'},
			{from: 'data/',to: 'data/'},
			{from: 'img/',to: 'img/'}
		])
		,new ImageminWebpackPlugin({
			disable: DEVELOPMENT,
			test: /\.(jpe?g|png|gif|svg)$/i,
			optipng: {
				optimizationLevel: 7,
			},
			pngquant: {
				quality: '65-90',
				speed: 4,
			},
			gifsicle: {
				optimizationLevel: 3,
			},
			svgo: {
				plugins: [{
					removeViewBox: false,
					removeEmptyAttrs: true,
				}],
			},
			jpegtran: {
				progressive: true,
			},
			plugins: [
				imageminMozjpeg({
					quality: 80,
				})
			]
		})

		/*,new CompressionWebpackPlugin({
			asset:'[path].gz[query]',
			algorithm: 'gzip',
			test: /\.(js|css|html|svg)$/,
			deleteOriginalAssets: true,
			threshold: 0,
			minRatio: 0.8
		})*/
	],

	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},

			{
				test: /\.pug$/,
				use:  ['pug-loader?pretty=true']
			},

			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: 'babel-loader'
					}
				]
			},

			{
				test: /\.(sass|scss)$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: ['css-loader','sass-loader', 'resolve-url-loader']
				})
			},

			{
				test: /\.(gif|png|jpe?g|svg|webp)$/i,
				use: [
					'file-loader?name=[path][name].[ext]'
				]
			}
		]
	}
};

module.exports = config;