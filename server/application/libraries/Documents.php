<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpWord\TemplateProcessor;

class Documents
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    private function check_template($auth, $id_template)
    {
        $template = $this->CI->crud_model->get_by_id('templates', 'id', $id_template);

        if(empty($template)) {
            $this->json['status'] = false;
            $this->json['message'] = "Template not found";
            return false;
        }

        $cant = true;
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return false;
        }

        return true;
    }

    public function add_document($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if(!$this->check_template($auth, $this->post['id_template'])) return $this->json;

        $insdata = array(
            'id_template' => $this->post['id_template'],
            'id_company' => $this->post['id_company'],
            'document_number' => $this->post['document_number'],
            'variables' => $this->post['variables'],
            'done' => $this->post['done'],
            'comment' => $this->post['comment'],
            'created' => date('Y-m-d')
        );

        $id = $this->CI->crud_model->insert_data('documents', $insdata);

        if ( strpos($this->post['document_number'], '{generated_document_id}') ){
			$upddata = array(
//				'id_template' => $this->post['id_template'],
//				'id_company' => $this->post['id_company'],
				'document_number' => str_replace('{generated_document_id}', $id, $this->post['document_number']),
//				'variables' => $this->post['variables'],
//				'done' => $this->post['done'],
//				'comment' => $this->post['comment']
			);
			$this->CI->crud_model->update_by_id('documents', 'id', $id, $upddata);
		}

        $this->json['status'] = true;
        $this->json['id'] = $id;
        $this->json['created'] = date('Y-m-d');

        return $this->json;
    }

    public function update_document($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if(!$this->check_template($auth, $this->post['id_template'])) return $this->json;

        $upddata = array(
            'id_template' => $this->post['id_template'],
            'id_company' => $this->post['id_company'],
			'document_number' => $this->post['document_number'],
            'variables' => $this->post['variables'],
			'comment' => $this->post['comment'],
            'done' => $this->post['done']
        );
        $this->CI->crud_model->update_by_id('documents', 'id', $this->post['id'], $upddata);

        $this->json['status'] = true;
        $this->json['message'] = "Document data updated";

        return $this->json;
    }

    public function get_document($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $document = $this->CI->crud_model->get_by_id('documents', 'id', $this->post['id']);

        if(empty($document)) {
            $this->json['status'] = false;
            $this->json['message'] = "Document not found";
            return $this->json;
        }

        if(!$this->check_template($auth, $document['id_template'])) return $this->json;

        if($document['id_company'] != null) {
			$this->json['extra']['id_company'] = $this->CI->crud_model->get_by_id('companies', 'id', $document['id_company']);
		}
		if($document['id_template'] != null) {
			$this->json['extra']['id_template'] = $this->CI->crud_model->get_by_id('templates', 'id', $document['id_template']);
		}

        $this->json['status'] = true;
        $this->json['data'] = $document;

        return $this->json;
    }

    public function get_document_list($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        /*
        $templates = $this->CI->crud_model->get_all_by_id('templates', 'id_company', $this->post['id_company']);

        $t_ids = array();
        foreach ($templates as $item) {
            $t_ids[] = $item['id'];
        }

        $this->json['status'] = true;
        if(!empty($t_ids))
            $this->json['data'] = $this->CI->crud_model->get_documents_list($t_ids, $this->post['count'], $this->post['offset']);
        else
            $this->json['data'] = array();
        */

        $this->json['status'] = true;

        if(isset($this->post['id_company'])) {

            $cant = true;
            if($auth['access'] == 0) {
                $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id_company']);
                if(empty($scope)) $cant = true;
                else $cant = false;
            }

            if($auth['access'] == 1) {
                $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
                $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id_company']);
                if(empty($scope)) $cant = true;
                else $cant = false;
            }

            if($cant) {
                $this->json['status'] = false;
                $this->json['message'] = "Access denied";
                return false;
            }

            $this->json['data'] = $this->CI->crud_model->get_documents_list($this->post['id_company'], $this->post['count'], $this->post['offset']);

			foreach ($this->json['data'] as $key => $val) {
			    $temp = $this->CI->crud_model->get_by_id('templates', 'id', $this->json['data'][$key]['id_template']);
				$this->json['data'][$key]['type'] = $temp['type'];
			}

        } else {

            $companies = array();

            if($auth['access'] == 0) {
                $companies = $this->CI->crud_model->get_shortcuts_companies_by_sa($auth['id_user'], 0, 0);
            }
            if($auth['access'] == 1) {
                $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
                $companies = $this->CI->crud_model->get_shortcuts_companies_by_sa($sa['id_user'], 0, 0);
            }

            if(!empty($companies)) {
                $t_ids = array();
                $c_ids = array();
                foreach ($companies as $item) {
                    $c_ids[] = $item['id'];
                }
                $templates = $this->CI->crud_model->get_all_by_array('templates', 'id_company', $c_ids);
                $t_ids = array();
                foreach ($templates as $item) {
                    $t_ids[] = $item['id'];
                }

            } else {
                $t_ids = array();
            }

            if(!empty($t_ids)){
                $this->json['data'] = $this->CI->crud_model->get_all_by_array('documents', 'id_template', $t_ids);

				foreach ($this->json['data'] as $key => $val) {
			    $temp = $this->CI->crud_model->get_by_id('templates', 'id', $this->json['data'][$key]['id_template']);
				$this->json['data'][$key]['type'] = $temp['type'];
				}

			} else{
				$this->json['data'] = array();
			}
        }

        return $this->json;
    }

    public function get_document_file($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $document = $this->CI->crud_model->get_by_id('documents', 'id', $this->post['id']);

        if(empty($document)) {
            $this->json['status'] = false;
            $this->json['message'] = "Document not found";
            return $this->json;
        }

        if(!$this->check_template($auth, $document['id_template'])) return $this->json;

        $result = './uploads/tmp/'.$document['id'].'_ready_'.time().'.docx';
        $vars = json_decode($document['variables'], true);
		$vars['crm_doc_number'] = $document['document_number'];

        $temp = $this->CI->crud_model->get_by_id('templates', 'id', $document['id_template']);

        $template = new TemplateProcessor($temp['path']);

        foreach ($vars as $key => $val) {
            $template->setValue($key, $val);
        }

        $template->saveAs($result);

        $this->CI->load->library('encrypt');
        $encrypted = $this->CI->encrypt->encode($result);
        $encrypted = strtr($encrypted,array('+' => '.', '=' => '-', '/' => '~'));

        $this->json['status'] = true;
        $this->json['url'] = base_url()."get_file/".$encrypted;

        return $this->json;
    }

    public function remove_document($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $document = $this->CI->crud_model->get_by_id('documents', 'id', $this->post['id']);

        if(empty($document)) {
            $this->json['status'] = false;
            $this->json['message'] = "Document not found";
            return $this->json;
        }

        if(!$this->check_template($auth, $document['id_template'])) return $this->json;

        $this->CI->crud_model->delete_by_id('documents', 'id', $this->post['id']);

        $this->json['status'] = true;
        $this->json['message'] = "Document removed";

        return $this->json;
    }

}