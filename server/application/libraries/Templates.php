<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpWord\TemplateProcessor;

class Templates
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function add_template($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if($_FILES['file']['error'] === 0) {
            $path = './uploads/templates/'.md5($this->post['id_company']);
            if(!file_exists($path)) mkdir($path,0755);
            $file = $path.'/'.time().'.docx';
            move_uploaded_file($_FILES['file']['tmp_name'], $file);

            $insdata = array(
                'id_company' => $this->post['id_company'],
                'name' => $this->post['name'],
                'type' => $this->post['type'],
                'description' => $this->post['description'],
                'path' => $file,
                'created' => date('Y-m-d')
            );

            $id = $this->CI->crud_model->insert_data('templates', $insdata);

            $this->json['status'] = true;
            $this->json['id'] = $id;
            $this->json['created'] = date('Y-m-d');

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Error loading file";
        }

        return $this->json;
    }

    public function update_template($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $template = $this->CI->crud_model->get_by_id('templates', 'id', $this->post['id']);

        if(empty($template)) {
            $this->json['status'] = false;
            $this->json['message'] = "Template not found";
            return $this->json;
        }

        $cant = true;
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

		if(!empty($_FILES['file'])) {
			if($_FILES['file']['error'] === 0) {
				move_uploaded_file($_FILES['file']['tmp_name'], $template['path']);
			}
        }

        $upddata = array(
            'name' => $this->post['name'],
            'type' => $this->post['type'],
            'description' => $this->post['description'],
        );
        $this->CI->crud_model->update_by_id('templates', 'id', $this->post['id'], $upddata);

        $this->json['status'] = true;
        $this->json['message'] = "Template data updated";

        return $this->json;
    }

    public function get_template($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $template = $this->CI->crud_model->get_by_id('templates', 'id', $this->post['id']);

        if(empty($template)) {
            $this->json['status'] = false;
            $this->json['message'] = "Template not found";
            return $this->json;
        }

        $cant = true;
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = true;
        $this->json['data'] = $template;
        //unset($this->json['data']['id_company']);
        unset($this->json['data']['path']);
        $variables = $this->get_template_variables($template['path']);
        if($variables == false) $this->json['data']['variables'] = 'Error. Exception catched';
        else $this->json['data']['variables'] = $variables;

		if($template['id_company'] != null) {
			$this->json['extra']['id_company'] = $this->CI->crud_model->get_by_id('companies', 'id', $template['id_company']);
		}

        return $this->json;
    }

    public function get_template_list($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $c_ids = array();
        $scopes = array();

        if($auth['access'] == 0) {
            $scopes = $this->CI->crud_model->get_scope_ids_companies_by_sa($auth['id_user']);
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scopes = $this->CI->crud_model->get_scope_ids_companies_by_sa($sa['id_user']);
        }

        foreach ($scopes as $item) {
            $c_ids[] = $item['id'];
        }

        $this->json['status'] = true;
        $this->json['data'] = $this->CI->crud_model->get_templates_list($c_ids, $this->post['count'], $this->post['offset']);

        return $this->json;
    }

    public function search_template($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $c_ids = array();
        $scopes = array();

        if($auth['access'] == 0) {
            $scopes = $this->CI->crud_model->get_scope_ids_companies_by_sa($auth['id_user']);
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scopes = $this->CI->crud_model->get_scope_ids_companies_by_sa($sa['id_user']);
        }

        foreach ($scopes as $item) {
            $c_ids[] = $item['id'];
        }

        $this->json['status'] = true;
        $this->json['data'] = $this->CI->crud_model->search_templates_list($c_ids, $this->post['search'], $this->post['count'], $this->post['offset']);

        return $this->json;
    }

    public function remove_template($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $template = $this->CI->crud_model->get_by_id('templates', 'id', $this->post['id']);

        if(empty($template)) {
            $this->json['status'] = false;
            $this->json['message'] = "Template not found";
            return $this->json;
        }

        $cant = true;
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $template['id_company']);
            if(empty($scope)) $cant = true;
            else $cant = false;
        }

        if($cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->CI->crud_model->delete_by_id('templates', 'id', $this->post['id']);
        unlink($template['path']);

        $path = './uploads/templates/'.md5($template['id_company']);
        if($this->is_dir_empty($path)) rmdir($path);

        $this->json['status'] = true;
        $this->json['message'] = "Template removed";

        return $this->json;
    }

    private function get_template_variables($file)
    {
        $template_vars = array();
        try {
            $template = new TemplateProcessor($file);
            foreach ($template->getVariables() as $key => $val) {
                $template_vars[] = $val;
            }
            return json_encode($template_vars);
        } catch (\PhpOffice\PhpWord\Exception\CopyFileException $e) {
            return false;
        } catch (\PhpOffice\PhpWord\Exception\CreateTemporaryFileException $e) {
            return false;
        }
    }

    private function is_dir_empty($dir) {
        $handle = opendir($dir);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return FALSE;
            }
        }
        return TRUE;
    }

}