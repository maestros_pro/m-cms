<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Companies
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function add_company($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $insdata = array(
            'description' => $this->post['description'],
            'entity_legal' => $this->post['entity_legal'],
            'entity_shortname' => $this->post['entity_shortname'],
            'entity_fullname' => $this->post['entity_fullname'],
            'entity_inn' => $this->post['entity_inn'],
            'entity_kpp' => $this->post['entity_kpp'],
            'entity_okpo' => $this->post['entity_okpo'],
            'entity_ogrn' => $this->post['entity_ogrn'],
            'entity_nds' => $this->post['entity_nds'],
            'entity_email' => $this->post['entity_email'],
            'entity_phone' => $this->post['entity_phone'],
            'entity_director' => $this->post['entity_director'],
            'entity_accountant' => $this->post['entity_accountant'],
            'entity_legal_address' => $this->post['entity_legal_address'],
            'entity_actual_address' => $this->post['entity_actual_address'],
            'bank_bik' => $this->post['bank_bik'],
            'bank_name' => $this->post['bank_name'],
            'bank_placement' => $this->post['bank_placement'],
            'bank_rcheck' => $this->post['bank_rcheck'],
            'bank_ccheck' => $this->post['bank_ccheck'],
            'created' => date('Y-m-d'),
            'img' => $this->post['img'],
            'ignored' => false
        );
        $id = $this->CI->crud_model->insert_data('companies', $insdata);

        $insdata = array(
            'id_user' => $auth['id_user'],
            'type_object' => 'company',
            'id_object' => $id
        );
        $this->CI->crud_model->insert_data('scopes', $insdata);

        $this->json['status'] = true;
        $this->json['id'] = $id;
        $this->json['created'] = date('d.m.Y');

        return $this->json;
    }

    public function update_company($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);

        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "Company not found or created by another SuperAdmin";
            return $this->json;
        }

        $upddata = array(
            'description' => $this->post['description'],
            'entity_legal' => $this->post['entity_legal'],
            'entity_shortname' => $this->post['entity_shortname'],
            'entity_fullname' => $this->post['entity_fullname'],
            'entity_inn' => $this->post['entity_inn'],
            'entity_kpp' => $this->post['entity_kpp'],
            'entity_okpo' => $this->post['entity_okpo'],
            'entity_ogrn' => $this->post['entity_ogrn'],
            'entity_nds' => $this->post['entity_nds'],
            'entity_email' => $this->post['entity_email'],
            'entity_phone' => $this->post['entity_phone'],
            'entity_director' => $this->post['entity_director'],
            'entity_accountant' => $this->post['entity_accountant'],
            'entity_legal_address' => $this->post['entity_legal_address'],
            'entity_actual_address' => $this->post['entity_actual_address'],
            'bank_bik' => $this->post['bank_bik'],
            'bank_name' => $this->post['bank_name'],
            'bank_placement' => $this->post['bank_placement'],
            'bank_rcheck' => $this->post['bank_rcheck'],
            'bank_ccheck' => $this->post['bank_ccheck'],
            'img' => $this->post['img'],
            'ignored' => $this->post['ignored']
        );
        $this->CI->crud_model->update_by_id('companies', 'id', $this->post['id'], $upddata);

        $this->json['status'] = true;
        $this->json['message'] = "Company data updated";

        return $this->json;
    }

    public function remove_company($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);

        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "Company not found or created by another SuperAdmin";
            return $this->json;
        }

        //По идее, согласно зависимым ключам, должны также удалиться из базы все зависимости
        $this->CI->crud_model->delete_by_id('companies', 'id', $this->post['id']);
        $this->CI->crud_model->delete_company_scope($this->post['id']);

        $this->json['status'] = true;
        $this->json['message'] = "Company removed";

        return $this->json;
    }

    public function get_company($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Company not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $this->json['data'] = $this->CI->crud_model->get_by_id('companies', 'id', $this->post['id']);
                $this->json['extra']['entity_director'] = $this->CI->crud_model->get_by_id('users', 'id', $this->json['data']['entity_director']);
                $this->json['extra']['entity_accountant'] = $this->CI->crud_model->get_by_id('users', 'id', $this->json['data']['entity_accountant']);
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Company not found or access denied";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $this->json['data'] = $this->CI->crud_model->get_by_id('companies', 'id', $this->post['id']);
				$this->json['extra']['entity_director'] = $this->CI->crud_model->get_by_id('users', 'id', $this->json['data']['entity_director']);
				$this->json['extra']['entity_accountant'] = $this->CI->crud_model->get_by_id('users', 'id', $this->json['data']['entity_accountant']);
				return $this->json;
            }
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_company_list($auth)
    {
        if($auth['access'] == 0) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_companies_by_sa($auth['id_user'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_companies_by_sa($sa['id_user'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function search_company($auth)
    {
        if($auth['access'] == 0) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_search_companies_by_sa($auth['id_user'], $this->post['search'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_search_companies_by_sa($sa['id_user'], $this->post['search'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_company_users($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Company not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $links_cu = $this->CI->crud_model->get_all_by_id('links_cu', 'id_company', $this->post['id']);
                $this->json['users'] = array();
                foreach ($links_cu as $item) {
                    $this->json['users'][] = $this->CI->crud_model->get_by_id('users', 'id', $item['id_user']);
                }
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Company not found or access denied";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $links_cu = $this->CI->crud_model->get_all_by_id('links_cu', 'id_company', $this->post['id']);
                $this->json['users'] = array();
                foreach ($links_cu as $item) {
                    $this->json['users'][] = $this->CI->crud_model->get_by_id('users', 'id', $item['id_user']);
                }
                return $this->json;
            }
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }
}