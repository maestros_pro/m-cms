<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function add_user($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if ( !empty($this->post['email']) ) {
			$check = $this->CI->crud_model->get_by_id('users', 'email', $this->post['email']);
		}

        if(!empty($check)) {
            $this->json['status'] = false;
            $this->json['message'] = "User with this email already exists";
            return $this->json;
        }

        $insdata = array(
            'position' => $this->post['position'],
            'first_name' => $this->post['first_name'],
            'second_name' => $this->post['second_name'],
            'third_name' => $this->post['third_name'],
            'phone' => $this->post['phone'],
            'email' => $this->post['email'],
            'gender' => $this->post['gender'],
            'description' => $this->post['description'],
            'img' => $this->post['img'],
            'birthday' => $this->post['birthday'],
            'active' => 0,
            'created' => date('Y-m-d')
        );
        $id = $this->CI->crud_model->insert_data('users', $insdata);

        $insdata = array(
            'id_user' => $auth['id_user'],
            'type_object' => 'user',
            'id_object' => $id
        );
        $this->CI->crud_model->insert_data('scopes', $insdata);

        if(is_array(json_decode($this->post['company_id']))) {
            foreach (json_decode($this->post['company_id']) as $id_c) {
                $insdata = array(
                    'id_user' => $id,
                    'id_company' => $id_c
                );
                $this->CI->crud_model->insert_data('links_cu', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['id'] = $id;
        $this->json['created'] = date('d.m.Y');

        return $this->json;
    }

    public function update_user($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "User not found or created by another SuperAdmin";
            return $this->json;
        }

		if ( !empty($this->post['email']) ) {
        	$check = $this->CI->crud_model->get_by_id('users', 'email', $this->post['email']);
        }

        if(!empty($check) AND $check['id'] != $this->post['id']) {
            $this->json['status'] = false;
            $this->json['message'] = "User with this email already exists";
            return $this->json;
        }

        $upddata = array(
            'position' => $this->post['position'],
            'first_name' => $this->post['first_name'],
            'second_name' => $this->post['second_name'],
            'third_name' => $this->post['third_name'],
            'phone' => $this->post['phone'],
            'email' => $this->post['email'],
            'gender' => $this->post['gender'],
            'description' => $this->post['description'],
            'img' => $this->post['img'],
            'birthday' => $this->post['birthday'],
            'active' => 0,
        );

        $this->CI->crud_model->update_by_id('users', 'id', $this->post['id'], $upddata);

        $this->CI->crud_model->delete_by_id('links_cu', 'id_user', $this->post['id']);

        if(is_array(json_decode($this->post['company_id']))) {
            foreach (json_decode($this->post['company_id']) as $id_c) {
                $insdata = array(
                    'id_user' => $this->post['id'],
                    'id_company' => $id_c
                );
                $this->CI->crud_model->insert_data('links_cu', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['message'] = "User data updated";

        return $this->json;
    }

    public function update_user_owner($auth) {
        if($auth['id_user'] != $this->post['id']) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $upddata = array(
            'position' => $this->post['position'],
            'first_name' => $this->post['first_name'],
            'second_name' => $this->post['second_name'],
            'third_name' => $this->post['third_name'],
            'phone' => $this->post['phone'],
            'email' => $this->post['email'],
            'img' => $this->post['img'],
            'birthday' => $this->post['birthday']
        );

        $this->CI->crud_model->update_by_id('users', 'id', $this->post['id'], $upddata);

        if(isset($this->post['password']) AND $this->post['password'] != '') {
            $upddata = array(
                'password' => $this->post['password']
            );
            $this->CI->crud_model->update_by_id('auth', 'id_user', $this->post['id'], $upddata);
        }

        $this->json['status'] = true;
        $this->json['message'] = "User data updated";

        return $this->json;
    }

    public function remove_user($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);

        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "User not found or created by another SuperAdmin";
            return $this->json;
        }

        //По идее, согласно зависимым ключам, должны также удалиться из базы все зависимости
        $this->CI->crud_model->delete_by_id('users', 'id', $this->post['id']);
        $this->CI->crud_model->delete_user_scope($this->post['id']);

        $this->json['status'] = true;
        $this->json['message'] = "User removed";

        return $this->json;
    }

    public function get_user($auth)
    {
        if($auth['access'] == 0) {
            if($auth['id_user'] == $this->post['id']) {
                $this->json['status'] = true;
                $this->json['data'] = $this->CI->crud_model->get_by_id('users', 'id', $this->post['id']);
                return $this->json;
            }

            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "User not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $this->json['data'] = $this->CI->crud_model->get_by_id('users', 'id', $this->post['id']);
                $temp = $this->CI->crud_model->get_by_id('auth', 'id_user', $this->post['id']);
                $this->json['data']['access'] = (empty($temp)) ? null : $temp['access'];
                $this->json['extra']['company_id'] = array();
                $this->json['data']['company_id'] = array();
                $companies = $this->CI->crud_model->get_all_by_id('links_cu', 'id_user', $this->post['id']);
                foreach ($companies as $id_c) {
                    $this->json['extra']['company_id'][] = $this->CI->crud_model->get_by_id('companies', 'id', $id_c['id_company']);
                    $this->json['data']['company_id'][] = $id_c['id_company'];
                }
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_user_by_sa($sa['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "User not found or access denied";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $this->json['data'] = $this->CI->crud_model->get_by_id('users', 'id', $this->post['id']);
                $this->json['extra']['company_id'] = array();
                $this->json['data']['company_id'] = array();
                $companies = $this->CI->crud_model->get_all_by_id('links_cu', 'id_user', $this->post['id']);
                foreach ($companies as $id_c) {
                    $this->json['extra']['company_id'][] = $this->CI->crud_model->get_by_id('companies', 'id', $id_c['id_company']);
                    $this->json['data']['company_id'][] = $id_c['id_company'];
                }
                return $this->json;
            }
        }

        if($auth['access'] == 2) {
			if($auth['id_user'] == $this->post['id']) {
				$this->json['status'] = true;
				$this->json['data'] = $this->CI->crud_model->get_by_id('users', 'id', $this->post['id']);
				return $this->json;
			}
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_user_list($auth)
    {
        if($auth['access'] == 0) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_users_by_sa($auth['id_user'], $this->post['count'], $this->post['offset']);
            for ($i = 0; $i < count($this->json['data']); $i++) {
            	$temp = $this->CI->crud_model->get_by_id('auth', 'id_user', $this->json['data'][$i]['id']);
            	if(!empty($temp)) $this->json['data'][$i]['access'] = $temp['access'];
			}

            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_users_by_sa($sa['id_user'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function search_user($auth)
    {
        if($auth['access'] == 0) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_search_users_by_sa($auth['id_user'], $this->post['search'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_shortcuts_search_users_by_sa($sa['id_user'], $this->post['search'], $this->post['count'], $this->post['offset']);
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }
}