<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function login()
    {
        $user = $this->CI->crud_model->get_auth($this->post['login'], $this->post['password']);
        if (empty($user)) {
            $this->json['status'] = false;
            $this->json['message'] = "Wrong login data";
        } else {
            $this->CI->session->set_userdata('login', $user['email']);
            $this->CI->session->set_userdata('password', $user['password']);
            $this->json['status'] = true;
            $this->json['message'] = "Correct login";
			$this->json['user'] = $this->CI->crud_model->get_by_id('users', 'id', $user['id_user']);
			switch($user['access']) {
				case 0:
					$this->json['rank_name'] = "Суперадминистратор";
					$this->json['rank'] = "0";
					break;
				case 1:
					$this->json['rank_name'] = "Администратор";
					$this->json['rank'] = "1";
					break;
				case 2:
					$this->json['rank_name'] = "Пользователь";
					$this->json['rank'] = "2";
					break;
			}
			if($user['access'] == 0) {
				$this->json['info'] = $user['info'];
			} else {
				$scope = $this->CI->crud_model->get_scope_sa_by_user($user['id_user']);
				$temp = $this->CI->crud_model->get_by_id('auth', 'id_user', $scope['id_user']);
				$this->json['info'] = $temp['info'];
			}
        }
        return $this->json;
    }

    public function logout()
    {
        $this->CI->session->unset_userdata('login');
        $this->CI->session->unset_userdata('password');
        $this->json['status'] = true;
        $this->json['message'] = "Correct logout";
        return $this->json;
    }

    public function auth_check($auth)
    {
        if (!empty($auth)) {
            $this->json['status'] = true;
            $this->json['message'] = "User authorized";
            $this->json['id'] = $auth['id_user'];
			$this->json['user'] = $this->CI->crud_model->get_by_id('users', 'id', $auth['id_user']);
            switch($auth['access']) {
                case 0:
                    $this->json['rank_name'] = "Суперадминистратор";
                    $this->json['rank'] = "0";
                    break;
                case 1:
                    $this->json['rank_name'] = "Администратор";
                    $this->json['rank'] = "1";
                    break;
                case 2:
                    $this->json['rank_name'] = "Пользователь";
                    $this->json['rank'] = "2";
                    break;
            }
            if($auth['access'] == 0) {
                $this->json['info'] = $auth['info'];
            } else {
                $scope = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
                $temp = $this->CI->crud_model->get_by_id('auth', 'id_user', $scope['id_user']);
                $this->json['info'] = $temp['info'];
            }
        } else {
            $this->json['status'] = false;
            $this->json['message'] = "User is not authorized";
        }
        return $this->json;
    }

    public function set_auth($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "User not found or created by another SuperAdmin";
                return $this->json;
            } elseif (!in_array($this->post['access'],array(1,2))){
                $this->json['status'] = false;
                $this->json['message'] = "Wrong access level parameter";
                return $this->json;
            } else {
                $user = $this->CI->crud_model->get_by_id('users', 'id', $this->post['id']);
                $insdata = array(
                    'id_user' => $this->post['id'],
                    'email' => $user['email'],
                    'password' => $this->post['password'],
                    'access' => $this->post['access']
                );
                $id = $this->CI->crud_model->insert_data('auth', $insdata);

                $this->json['status'] = true;
                return $this->json;
            }
        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }
    }

    public function set_info_auth($auth) {
        if($auth['id_user'] != $this->post['id']) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if(isset($this->post['info'])) {
            $upddata = array(
                'info' => $this->post['info']
            );
            $this->CI->crud_model->update_by_id('auth', 'id_user', $this->post['id'], $upddata);
        }

        $this->json['status'] = true;
        $this->json['message'] = "Info updated";

        return $this->json;
    }

    public function unset_auth($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            $check = $this->CI->crud_model->get_by_id('auth', 'id_user', $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "User not found or created by another SuperAdmin";
                return $this->json;
            } elseif ($check['access'] === 0) {
                $this->json['status'] = false;
                $this->json['message'] = "You can`t unset authentication for SuperAdmin";
                return $this->json;
            } else {
                $this->CI->crud_model->delete_by_id('auth', 'id_user', $this->post['id']);

                $this->json['status'] = true;
                return $this->json;
            }
        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }
    }
}