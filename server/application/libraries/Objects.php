<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Objects
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function add_object($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $insdata = array(
            'id_company' => $this->post['id_company'],
            'name' => $this->post['name'],
            'address' => $this->post['address'],
            'description' => $this->post['description'],
            'img' => $this->post['img'],
            'coordinates' => $this->post['coordinates'],
            'created' => date('Y-m-d')
        );
        $id = $this->CI->crud_model->insert_data('objects', $insdata);

        if(is_array(json_decode($this->post['users_ids']))) {
            foreach (json_decode($this->post['users_ids']) as $id_u) {
                $insdata = array(
                    'id_object' => $id,
                    'id_user' => $id_u
                );
                $this->CI->crud_model->insert_data('links_ou', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['id'] = $id;
        $this->json['created'] = date('d.m.Y');

        return $this->json;
    }

    public function update_object($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
        $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $object['id_company']);

        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "Object not found or created by another SuperAdmin";
            return $this->json;
        }

        $upddata = array(
            'id_company' => $this->post['id_company'],
            'name' => $this->post['name'],
            'address' => $this->post['address'],
            'description' => $this->post['description'],
            'img' => $this->post['img'],
			'coordinates' => $this->post['coordinates']
        );
        $this->CI->crud_model->update_by_id('objects', 'id', $this->post['id'], $upddata);

        $this->CI->crud_model->delete_by_id('links_ou', 'id_object', $this->post['id']);
        if(is_array(json_decode($this->post['users_ids']))) {
            foreach (json_decode($this->post['users_ids']) as $id_u) {
                $insdata = array(
                    'id_object' => $this->post['id'],
                    'id_user' => $id_u
                );
                $this->CI->crud_model->insert_data('links_ou', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['message'] = "Object data updated";

        return $this->json;
    }

    public function remove_object($auth)
    {
        if($auth['access'] != 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
        $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $object['id_company']);

        if(empty($scope)) {
            $this->json['status'] = false;
            $this->json['message'] = "Object not found or created by another SuperAdmin";
            return $this->json;
        }

        //По идее, согласно зависимым ключам, должны также удалиться из базы все зависимости
        $this->CI->crud_model->delete_by_id('objects', 'id', $this->post['id']);

        $this->json['status'] = true;
        $this->json['message'] = "Object removed";

        return $this->json;
    }

    public function get_object($auth)
    {
        if($auth['access'] == 0) {
            $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $object['id_company']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Object not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
                $object['users_ids'] = $this->get_users_ids_for_object($object['id']);
                $this->json['data'] = $object;
                if(!empty($object['users_ids'])) $this->json['extra']['users_ids'] = $this->CI->crud_model->get_all_by_array('users', 'id', $object['users_ids']);
                else $this->json['extra']['users_ids'] = array();
                $this->json['extra']['id_company'] = $this->CI->crud_model->get_by_id('companies', 'id', $object['id_company']);
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $object['id_company']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Object not found or access denied";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $object = $this->CI->crud_model->get_by_id('objects', 'id', $this->post['id']);
                $object['users_ids'] = $this->get_users_ids_for_object($object['id']);
                $this->json['data'] = $object;
                if(!empty($object['users_ids'])) $this->json['extra']['users_ids'] = $this->CI->crud_model->get_all_by_array('users', 'id', $object['users_ids']);
                else $this->json['extra']['users_ids'] = array();
                $this->json['extra']['id_company'] = $this->CI->crud_model->get_by_id('companies', 'id', $object['id_company']);
                return $this->json;
            }
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    private function get_users_ids_for_object($id_object) {
        $charge_ids = array();
        $temp = $this->CI->crud_model->get_all_by_id('links_ou', 'id_object', $id_object);
        foreach ($temp as $t) {
            $charge_ids[] = $t['id_user'];
        }
        return $charge_ids;
    }

    public function get_object_list($auth)
    {
        if($auth['access'] == 0) {
            $temp = $this->CI->crud_model->get_shortcuts_companies_by_sa($auth['id_user'], 0, 0);
            $c_ids = array();
            $c_ids[] = 0;   //чтобы не было ошибки БД
            foreach($temp as $t) {
                $c_ids[] = $t['id'];
            }
            $this->json['status'] = true;

            $objects = $this->CI->crud_model->get_objects_list_by_company($c_ids, $this->post['count'], $this->post['offset']);
            for($i = 0; $i < count($objects); $i++) {
                $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
            }

            $this->json['data'] = $objects;
            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $temp = $this->CI->crud_model->get_shortcuts_companies_by_sa($sa['id_user'], 0, 0);
            $c_ids = array();
            $c_ids[] = 0;   //чтобы не было ошибки БД
            foreach($temp as $t) {
                $c_ids[] = $t['id'];
            }
            $this->json['status'] = true;
            $objects = $this->CI->crud_model->get_objects_list_by_company($c_ids, $this->post['count'], $this->post['offset']);
            for($i = 0; $i < count($objects); $i++) {
                $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
            }
            $this->json['data'] = $objects;
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_object_by_company($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Objects not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $objects = $this->CI->crud_model->get_objects_list_by_company(array($this->post['id']), $this->post['count'], $this->post['offset']);
                for($i = 0; $i < count($objects); $i++) {
                    $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
                }
                $this->json['data'] = $objects;
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Objects not found or access denied";
                return $this->json;
            } else {
                $this->json['status'] = true;
                $objects = $this->CI->crud_model->get_objects_list_by_company(array($this->post['id']), $this->post['count'], $this->post['offset']);
                for($i = 0; $i < count($objects); $i++) {
                    $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
                }
                $this->json['data'] = $objects;
                return $this->json;
            }
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_object_by_user($auth)
    {
        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Objects not found or created by another SuperAdmin";
                return $this->json;
            } else {
                $temp = $this->CI->crud_model->get_all_by_id('links_ou', 'id_user', $this->post['id']);
                $o_ids = array();
                $o_ids[] = 0;   //чтобы не было ошибки БД
                foreach($temp as $t) {
                    $o_ids[] = $t['id_object'];
                }
                $this->json['status'] = true;
                $objects = $this->CI->crud_model->get_objects_list_by_ids($o_ids, $this->post['count'], $this->post['offset']);
                for($i = 0; $i < count($objects); $i++) {
                    $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
                }
                $this->json['data'] = $objects;
                return $this->json;
            }
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_user_by_sa($sa['id_user'], $this->post['id']);
            if(empty($scope)) {
                $this->json['status'] = false;
                $this->json['message'] = "Objects not found or access denied";
                return $this->json;
            } else {
                $temp = $this->CI->crud_model->get_all_by_id('links_ou', 'id_user', $this->post['id']);
                $o_ids = array();
                $o_ids[] = 0;   //чтобы не было ошибки БД
                foreach($temp as $t) {
                    $o_ids[] = $t['id_object'];
                }
                $this->json['status'] = true;
                $objects = $this->CI->crud_model->get_objects_list_by_ids($o_ids, $this->post['count'], $this->post['offset']);
                for($i = 0; $i < count($objects); $i++) {
                    $objects[$i]['users_ids'] = $this->get_users_ids_for_object($objects[$i]['id']);
                }
                $this->json['data'] = $objects;
                return $this->json;
            }
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

}