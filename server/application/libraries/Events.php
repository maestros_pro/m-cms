<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Events
{
    private $json;
    private $post;
    private $CI;

    public function __construct($post)
    {
        $this->CI = &get_instance();
        $this->json = array();
        $this->post = $post;
    }

    public function add_event($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $insdata = array(
            'event_date' => $this->post['event_date'],
            'type' => $this->post['type'],
            'description' => $this->post['description'],
//            'comment' => '',
//			'confirm' => 0,
            'notify' => (empty($this->post['notify'])) ? NULL : $this->post['notify'],
            'id_company' => (empty($this->post['id_company'])) ? NULL : $this->post['id_company'],
            'id_object' => (empty($this->post['id_object']))? NULL : $this->post['id_object'],
            'created' => date('Y-m-d'),
            'created_by_user' => $auth['id_user']
        );

        $id = $this->CI->crud_model->insert_data('events', $insdata);

        if(is_array(json_decode($this->post['users_ids']))) {
            foreach (json_decode($this->post['users_ids']) as $id_u) {
                $insdata = array(
                    'id_event' => $id,
                    'id_user' => $id_u
                );
                $this->CI->crud_model->insert_data('links_eu', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['id'] = $id;
        $this->json['created'] = date('Y-m-d');
        $this->json['data']['event_past'] = (strtotime($this->post['event_date'])<strtotime(date('Y-m-d')));
        $this->json['created_by_user'] = $auth['id_user'];

        return $this->json;
    }

    public function update_event($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }


        $event = $this->CI->crud_model->get_by_id('events', 'id', $this->post['id']);

        $cant = true;
        if($auth['id_user'] == $event['created_by_user']) $cant = false;
        elseif ($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $event['created_by_user']);
            if(!empty($scope)) $cant = false;
        }

        if(empty($event) OR $cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Event not found or created by another Admin";
            return $this->json;
        }

        $upddata = array(
            'event_date' => $this->post['event_date'],
            'type' => $this->post['type'],
            'description' => $this->post['description'],
//            'comment' => $this->post['comment'],
//			'confirm' => $this->post['confirm'],
            'notify' => (empty($this->post['notify'])) ? NULL : $this->post['notify'],
            'id_company' => (empty($this->post['id_company'])) ? NULL : $this->post['id_company'],
            'id_object' => (empty($this->post['id_object']))? NULL : $this->post['id_object'],
        );
        $this->CI->crud_model->update_by_id('events', 'id', $this->post['id'], $upddata);

        if(is_array(json_decode($this->post['users_ids']))) {
            //echo "ok";
            $this->CI->crud_model->delete_by_id('links_eu', 'id_event', $this->post['id']);
            foreach (json_decode($this->post['users_ids']) as $id_u) {
                $insdata = array(
                    'id_event' => $this->post['id'],
                    'id_user' => $id_u
                );
                $this->CI->crud_model->insert_data('links_eu', $insdata);
            }
        }

        $this->json['status'] = true;
        $this->json['message'] = "Event data updated";

        return $this->json;
    }

    public function confirm_event($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }


        $event = $this->CI->crud_model->get_by_id('events', 'id', $this->post['id']);

        $cant = true;
        if($auth['id_user'] == $event['created_by_user']) $cant = false;
        elseif ($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $event['created_by_user']);
            if(!empty($scope)) $cant = false;
        }

        if(empty($event) OR $cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Event not found or created by another Admin";
            return $this->json;
        }

        $upddata = array(
            'confirm' => $this->post['confirm'],
            'comment' => $this->post['comment']
        );
        $this->CI->crud_model->update_by_id('events', 'id', $this->post['id'], $upddata);

        $this->json['status'] = true;
        $this->json['message'] = "Event confirm added";

        return $this->json;
    }

    public function get_event($auth)
    {
        $event = $this->CI->crud_model->get_by_id('events', 'id', $this->post['id']);

        $cant = true;
        if($auth['id_user'] == $event['created_by_user']) $cant = false;
        elseif ($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $event['created_by_user']);
            if(!empty($scope)) $cant = false;
        } elseif ($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_user_by_sa($sa['id_user'], $auth['id_user']);
            if(!empty($scope)) $cant = false;
        } else {
            $tmp = $this->CI->crud_model->get_event_by_user($auth['id_user'], $this->post['id']);
            if(!empty($tmp)) $cant = false;
        }

        if(empty($event) OR $cant) {
            $this->json['status'] = false;
            $this->json['message'] = "Event not found or created by another Admin";
            return $this->json;
        }

        $this->json['status'] = true;
        $this->json['data'] = $event;
        $users = $this->CI->crud_model->get_all_by_id('links_eu', 'id_event', $this->post['id']);
        foreach ($users as $id_u) {
            $this->json['extra']['users_ids'][] = $this->CI->crud_model->get_by_id('users', 'id', $id_u['id_user']);
            $this->json['data']['users_ids'][] = $id_u['id_user'];
        }
        if($event['id_company'] != null) {
            $this->json['extra']['id_company'] = $this->CI->crud_model->get_by_id('companies', 'id', $event['id_company']);
        }
        if($event['id_object'] != null) {
            $this->json['extra']['id_object'] = $this->CI->crud_model->get_by_id('objects', 'id', $event['id_object']);
        }
		$this->json['extra']['created_by_user'] = $this->CI->crud_model->get_by_id('users', 'id', $event['created_by_user']);
        $this->json['data']['event_past'] = (strtotime($event['event_date'])<strtotime(date('Y-m-d')));

        return $this->json;
    }

    public function get_event_list($auth)
    {
        if($auth['access'] > 1) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        if($auth['access'] == 0) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_events_by_sa($auth['id_user'], $this->post['from'], $this->post['to']);
            return $this->json;
        }

        if($auth['access'] == 1) {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_events_by_sa($sa['id_user'], $this->post['from'], $this->post['to']);
            return $this->json;
        }

        if($auth['access'] == 2) {
            $this->json['status'] = false;
            $this->json['message'] = "Access denied";
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_event_by_user($auth)
    {
        $can = false;

        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        } else {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_user_by_sa($sa['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        }

        if($can) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_events_by_user($this->post['id'], $this->post['from'], $this->post['to']);
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_event_by_company($auth)
    {
        $can = false;

        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        } else {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        }

        if($can) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_events_by_company($this->post['id'], $this->post['from'], $this->post['to']);
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function get_event_by_object($auth)
    {
        $can = false;

        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_company_by_sa($auth['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        } else {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_company_by_sa($sa['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        }

        if($can) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_events_by_object($this->post['id'], $this->post['from'], $this->post['to']);
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

    public function remove_event($auth)
    {
		if($auth['access'] > 1) {
			$this->json['status'] = false;
			$this->json['message'] = "Access denied";
			return $this->json;
		}

		$this->CI->crud_model->delete_by_id('events', 'id', $this->post['id']);

		$this->json['status'] = true;
		$this->json['message'] = "Event removed";

		return $this->json;
    }

    public function get_overdue_event_by_user($auth)
    {
        $can = false;

        if($auth['access'] == 0) {
            $scope = $this->CI->crud_model->get_scope_user_by_sa($auth['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        } else {
            $sa = $this->CI->crud_model->get_scope_sa_by_user($auth['id_user']);
            $scope = $this->CI->crud_model->get_scope_user_by_sa($sa['id_user'], $this->post['id']);
            if(!empty($scope)) $can = true;
        }

        if($can) {
            $this->json['status'] = true;
            $this->json['data'] = $this->CI->crud_model->get_overdue_events_by_user($this->post['id']);
            return $this->json;
        }

        $this->json['status'] = false;
        $this->json['message'] = "Wrong access data";
        return $this->json;
    }

}