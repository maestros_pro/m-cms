<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->clean_sessions();
    }

    //Чистка сессий (на всякий случай, чтобы не плодились)
    public function clean_sessions()
    {
        $this->db->where('timestamp <', (time() - $this->config->item('sess_expiration')));
        $this->db->delete('ci_sessions');
    }

    /*-------------------------------------------------General CRUD---------------------------------------------------*/

    public function get_status_db()
    {
        $query = $this->db->query("SHOW TABLE STATUS");
        return $query->result_array();
    }

    public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort($table, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_limit_sort($limit, $table, $column, $sort)
    {
        $this->db->limit($limit);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort_pagination($table, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->row_array();
    }

    public function get_all_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_like_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->like($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort_pagination($table, $field_id, $id, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array_sort($table, $field_id, $array, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function update_by_id($table, $field_id, $id, $upddata)
    {
        $this->db->where($field_id, $id);
        $this->db->update($table, $upddata);
    }

    public function insert_data($table, $insdata)
    {
        $this->db->insert($table, $insdata);
        return $this->db->insert_id();
    }

    public function count_all($table)
    {
        return $this->db->count_all($table);
    }

    public function count_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_like_id($table, $field_id, $id)
    {
        $this->db->like($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function delete_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->delete($table);
    }

    public function delete_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $this->db->delete($table);
    }

    /*-------------------------------------------------Authentication-------------------------------------------------*/

    public function get_auth($login, $password)
    {
        $this->db->where('email', $login);
        $this->db->where('password', $password);
        $query = $this->db->get('auth');
        return $query->row_array();
    }

    /*-------------------------------------------------Companies------------------------------------------------------*/

    public function get_scope_company_by_sa($id_u, $id_c)
    {
        $this->db->where('id_user', $id_u);
        $this->db->where('id_object', $id_c);
        $this->db->where('type_object', 'company');
        $query = $this->db->get('scopes');
        return $query->row_array();
    }

    public function get_shortcuts_companies_by_sa($id_u, $count, $offset)
    {
        $this->db->select('id, entity_fullname, entity_legal, img, ignored');
        $this->db->from('companies');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->select('id_object as id, img, entity_fullname, entity_legal, ignored');
        $this->db->where('id_user', $id_u);
        $this->db->where('type_object', 'company');
        $this->db->from('scopes');
        $this->db->join("($subquery)  c", "id_object = c.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_shortcuts_search_companies_by_sa($id_u, $search, $count, $offset)
    {
        $this->db->select('id, entity_fullname, img, entity_legal');
        $this->db->like('entity_fullname', $search);
        $this->db->from('companies');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->select('id_object as id, img, entity_fullname, entity_legal');
        $this->db->where('id_user', $id_u);
        $this->db->where('type_object', 'company');
        $this->db->from('scopes');
        $this->db->join("($subquery)  c", "id_object = c.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_company_scope($id)
    {
        $this->db->where('type_object', 'company');
        $this->db->where('id_object', $id);
        $this->db->delete('scopes');
    }

    /*-------------------------------------------------Users----------------------------------------------------------*/

    public function get_scope_sa_by_user($id_u)
    {
        $this->db->where('id_object', $id_u);
        $this->db->where('type_object', 'user');
        $query = $this->db->get('scopes');
        return $query->row_array();
    }

    public function get_scope_user_by_sa($id_a, $id_u)
    {
        $this->db->where('id_user', $id_a);
        $this->db->where('id_object', $id_u);
        $this->db->where('type_object', 'user');
        $query = $this->db->get('scopes');
        return $query->row_array();
    }

    public function get_shortcuts_users_by_sa($id_u, $count, $offset)
    {
        $this->db->select('id, first_name, second_name, third_name, img, position');
        $this->db->from('users');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->select('id_object as id, img, first_name, second_name, third_name, position');
        $this->db->where('id_user', $id_u);
        $this->db->where('type_object', 'user');
        $this->db->from('scopes');
        $this->db->join("($subquery)  c", "id_object = c.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_shortcuts_search_users_by_sa($id_u, $search, $count, $offset)
    {
        $this->db->select('id, first_name, second_name, third_name, img, position');
        $this->db->like('first_name', $search);
        $this->db->or_like('second_name', $search);
        $this->db->or_like('third_name', $search);
        $this->db->from('users');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->select('id_object as id, img, first_name, second_name, third_name, position');
        $this->db->where('id_user', $id_u);
        $this->db->where('type_object', 'user');
        $this->db->from('scopes');
        $this->db->join("($subquery)  c", "id_object = c.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_user_scope($id)
    {
        $this->db->where('type_object', 'user');
        $this->db->where('id_object', $id);
        $this->db->delete('scopes');
    }

    /*-------------------------------------------------Objects--------------------------------------------------------*/
    public function get_objects_list_by_company($c_ids, $count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->where_in('id_company', $c_ids);
        $query = $this->db->get('objects');
        return $query->result_array();
    }

    public function get_objects_list_by_ids($o_ids, $count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->where_in('id', $o_ids);
        $query = $this->db->get('objects');
        return $query->result_array();
    }
    /*-------------------------------------------------Events---------------------------------------------------------*/
    public function get_events_by_sa($id_u, $from, $to)
    {
        /*
        $this->db->select('id_object as u_id');
        $this->db->where('type_object', 'user');
        $this->db->where('id_user', $id_u);
        $this->db->from('scopes');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();
        */
        $subquery = "SELECT `id_object` as `u_id` FROM `scopes` WHERE `type_object` = 'user' AND `id_user` = '".$id_u."' UNION SELECT `id_user` as `u_id` FROM `auth` WHERE `id_user` = '".$id_u."' ";

        $this->db->where('event_date  >=', $from);
        $this->db->where('event_date  <=', $to);
        $this->db->join("($subquery)  c", "created_by_user = c.u_id");
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function get_events_by_user($id_u, $from, $to)
    {
        $this->db->select('id_event');
        $this->db->where('id_user', $id_u);
        $this->db->from('links_eu');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        $this->db->where('event_date  >=', $from);
        $this->db->where('event_date  <=', $to);
        $this->db->join("($subquery)  c", "id = c.id_event");
        $this->db->order_by('event_date', 'ASC');
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function get_event_by_user($id_u, $id_e)
    {
        $this->db->where('id_user', $id_u);
        $this->db->where('id_event', $id_e);
        $query = $this->db->get('links_eu');
        return $query->row_array();
    }

    public function get_events_by_company($id_c, $from, $to)
    {
        $this->db->where('id_company', $id_c);
        $this->db->where('event_date  >=', $from);
        $this->db->where('event_date  <=', $to);
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function get_events_by_object($id_o, $from, $to)
    {
        $this->db->where('id_object', $id_o);
        $this->db->where('event_date  >=', $from);
        $this->db->where('event_date  <=', $to);
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function cron_get_hourly_events()
    {
        $this->db->where('event_date', date("Y-m-d"));
        $this->db->where('confirm', 0);
        $this->db->where('notify', date("H:00:00"));
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function get_overdue_events_by_user($id_u)
    {
        $this->db->select('id_event');
        $this->db->where('id_user', $id_u);
        $this->db->from('links_eu');
        $subquery = $this->db->get_compiled_select();
        $this->db->reset_query();

        $this->db->where('event_date  <', date('Y-m-d'));
        $this->db->where('confirm', 0);
        $this->db->join("($subquery)  c", "id = c.id_event");
        $this->db->order_by('event_date', 'ASC');
        $query = $this->db->get('events');
        return $query->result_array();
    }

    /*-------------------------------------------------Templates------------------------------------------------------*/

    public function get_scope_ids_companies_by_sa($id_u)
    {
        $this->db->select('id_object as id');
        $this->db->where('id_user', $id_u);
        $this->db->where('type_object', 'company');
        $query = $this->db->get('scopes');
        return $query->result_array();
    }

    public function get_templates_list($c_ids, $count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->where_in('id_company', $c_ids);
        $query = $this->db->get('templates');
        return $query->result_array();
    }

    public function search_templates_list($c_ids, $search, $count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->where_in('id_company', $c_ids);
        $this->db->like('name', $search);
        $query = $this->db->get('templates');
        return $query->result_array();
    }

    /*-------------------------------------------------Documents------------------------------------------------------*/

    public function get_documents_list($id_company, $count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->where('id_company', $id_company);
        $query = $this->db->get('documents');
        return $query->result_array();
    }
}