<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller
{
    private $auth;
    private $json;

    public function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->json = array();
        $this->auth = array();

        $this->auth = $this->crud_model->get_auth($this->session->userdata('login'), $this->session->userdata('password'));
    }

    /**
     * Контроллер запросов
     * Распределяет запросы по классам
     */
    public function index()
    {
        if (!isset($_POST['request'])) {
            $this->load->view('app');
        } else {
            $post = $this->input->post(NULL, FALSE);

            $this->allow_access($post['request']);

            $this->load_library($post);

            switch ($post['request']) {

                case 'login':
                    $this->json = $this->authentication->login();
                    break;
                case 'logout':
                    $this->json = $this->authentication->logout();
                    break;
                case 'authCheck':
                    $this->json = $this->authentication->auth_check($this->auth);
                    break;
                case 'setAuth':
                    $this->json = $this->authentication->set_auth($this->auth);
                    break;
                case 'setInfoAuth':
                    $this->json = $this->authentication->set_info_auth($this->auth);
                    break;
                case 'unsetAuth':
                    $this->json = $this->authentication->unset_auth($this->auth);
                    break;

                case 'addCompany':
                    $this->json = $this->companies->add_company($this->auth);
                    break;
                case 'updateCompany':
                    $this->json = $this->companies->update_company($this->auth);
                    break;
                case 'removeCompany':
                    $this->json = $this->companies->remove_company($this->auth);
                    break;
                case 'getCompany':
                    $this->json = $this->companies->get_company($this->auth);
                    break;
                case 'getCompanyList':
                    $this->json = $this->companies->get_company_list($this->auth);
                    break;
                case 'searchCompany':
                    $this->json = $this->companies->search_company($this->auth);
                    break;
                case 'getCompanyUsers':
                    $this->json = $this->companies->get_company_users($this->auth);
                    break;

                case 'addUser':
                    $this->json = $this->users->add_user($this->auth);
                    break;
                case 'updateUser':
                    $this->json = $this->users->update_user($this->auth);
                    break;
                case 'updateUserOwner':
                    $this->json = $this->users->update_user_owner($this->auth);
                    break;
                case 'removeUser':
                    $this->json = $this->users->remove_user($this->auth);
                    break;
                case 'getUser':
                    $this->json = $this->users->get_user($this->auth);
                    break;
                case 'getUserList':
                    $this->json = $this->users->get_user_list($this->auth);
                    break;
                case 'searchUser':
                    $this->json = $this->users->search_user($this->auth);
                    break;

                case 'addObject':
                    $this->json = $this->objects->add_object($this->auth);
                    break;
                case 'updateObject':
                    $this->json = $this->objects->update_object($this->auth);
                    break;
                case 'removeObject':
                    $this->json = $this->objects->remove_object($this->auth);
                    break;
                case 'getObject':
                    $this->json = $this->objects->get_object($this->auth);
                    break;
                case 'getObjectList':
                    $this->json = $this->objects->get_object_list($this->auth);
                    break;
                case 'getObjectByCompany':
                    $this->json = $this->objects->get_object_by_company($this->auth);
                    break;
                case 'getObjectByUser':
                    $this->json = $this->objects->get_object_by_user($this->auth);
                    break;

                case 'addEvent':
                    $this->json = $this->events->add_event($this->auth);
                    break;
                case 'updateEvent':
                    $this->json = $this->events->update_event($this->auth);
					break;
                case 'confirmEvent':
                    $this->json = $this->events->confirm_event($this->auth);
                    break;
                case 'getEvent':
                    $this->json = $this->events->get_event($this->auth);
                    break;
                case 'getEventList':
                    $this->json = $this->events->get_event_list($this->auth);
                    break;
                case 'getEventByUser':
                    $this->json = $this->events->get_event_by_user($this->auth);
                    break;
                case 'getEventByCompany':
                    $this->json = $this->events->get_event_by_company($this->auth);
                    break;
                case 'getEventByObject':
                    $this->json = $this->events->get_event_by_object($this->auth);
                    break;
				case 'removeEvent':
					$this->json = $this->events->remove_event($this->auth);
					break;
                case 'getOverdueEventByUser':
                    $this->json = $this->events->get_overdue_event_by_user($this->auth);
                    break;

                case 'addTemplate':
                    $this->json = $this->templates->add_template($this->auth);
                    break;
                case 'updateTemplate':
                    $this->json = $this->templates->update_template($this->auth);
                    break;
                case 'getTemplate':
                    $this->json = $this->templates->get_template($this->auth);
                    break;
                case 'getTemplateList':
                    $this->json = $this->templates->get_template_list($this->auth);
                    break;
                case 'searchTemplate':
                    $this->json = $this->templates->search_template($this->auth);
                    break;
                case 'removeTemplate':
                    $this->json = $this->templates->remove_template($this->auth);
                    break;

                case 'addDocument':
                    $this->json = $this->documents->add_document($this->auth);
                    break;
                case 'updateDocument':
                    $this->json = $this->documents->update_document($this->auth);
                    break;
                case 'getDocument':
                    $this->json = $this->documents->get_document($this->auth);
                    break;
                case 'getDocumentList':
                    $this->json = $this->documents->get_document_list($this->auth);
                    break;
                case 'getDocumentFile':
                    $this->json = $this->documents->get_document_file($this->auth);
                    break;
                case 'removeDocument':
                    $this->json = $this->documents->remove_document($this->auth);
                    break;

                default:
                    $this->json['status'] = false;
                    $this->json['message'] = "Wrong request";
            }

            echo json_encode($this->json);
        }
    }

    private function allow_access($request)
    {
        if ($request != 'login' AND $request != 'logout' AND $request != 'authCheck') {
            if (empty($this->auth)) {
                $this->json['status'] = false;
                $this->json['message'] = "User is not authorized";
                exit(json_encode($this->json));
            }
        }
    }

    private function load_library($post)
    {
        if ($post['request'] == 'login' OR $post['request'] == 'logout' OR $post['request'] == 'authCheck') $this->load->library('authentication', $post);
        if (mb_strpos($post['request'], "Auth") !== false) $this->load->library('authentication', $post);
        if (mb_strpos($post['request'], "Company") !== false) $this->load->library('companies', $post);
        if (mb_strpos($post['request'], "User") !== false) $this->load->library('users', $post);
        if (mb_strpos($post['request'], "Object") !== false) $this->load->library('objects', $post);
        if (mb_strpos($post['request'], "Event") !== false) $this->load->library('events', $post);
        if (mb_strpos($post['request'], "Template") !== false) $this->load->library('templates', $post);
        if (mb_strpos($post['request'], "Document") !== false) $this->load->library('documents', $post);
    }
}
