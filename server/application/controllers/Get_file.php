<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_file extends CI_Controller {

	public function index($encrypted = '')
	{
        $this->load->library('encrypt');
        $file = $this->encrypt->decode(strtr($encrypted, array('.' => '+', '-' => '=', '~' => '/')));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        flush();
        readfile($file);

        unlink($file);
	}
}
