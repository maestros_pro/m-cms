<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function index()
    {

    }

    public function hourly_events()
    {
        $events = $this->crud_model->cron_get_hourly_events();
        foreach ($events as $event) {
            $links_eu = $this->crud_model->get_all_by_id('links_eu', 'id_event', $event['id']);
            foreach($links_eu as $item) {
                $user = $this->crud_model->get_by_id('users', 'id', $item['id_user']);
                if(!empty($this->crud_model->get_by_id('auth', 'id_user', $item['id_user']))) $this->send_reminder_mail($user, $event);
            }
        }
    }

    private function send_reminder_mail($user, $event)
    {
        if(!empty($user['email'])) {
            $message = '<html>';
            $message .= '<body style="font-family:Arial,Helvetica,sans-serif; font-size: 12px; padding:10px;">';
            $message .= '<p style="margin-top:10px;">Здравствуйте, '.$user['first_name'].' '.$user['third_name'].'!</p>';
            $message .= '<p style="margin-top:10px;">Напоминание о событии.</p>';
            $message .= '<p style="margin-top:10px;">Вы отмечены, как участник события: '.$event['event_date'].'.</p>';
			$message .= '<p style="margin-top:10px;">'.$event['description'].'</p>';
			$message .= '<p style="margin-top:10px;">Подробности по ссылке: <a href="https://cms.maestros.ru/event/'.$event['id'].'"></a></p>';
            $message .= '<pre style="margin-top:10px;">'.print_r($event, true).'</pre>';
            $message .= '<p style="margin-top:10px;"><small>Письмо создано автоматически. Отвечать на него не надо.</small></p></body></html>';

            $headers = "Content-type:text/html;charset=utf-8 \r\n";
            $headers .= "From:Робот cms.maestros.ru <noreply@cms.maestros.ru>\r\n";

            mail($user['email'], 'Напоминание о событии', $message, $headers);
        }
    }
}
