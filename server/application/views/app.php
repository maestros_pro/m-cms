<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" href="<?=base_url();?>img/favicon/favicon.ico" type="image/x-icon"/>
	<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url();?>img/favicon/apple-touch-icon.png"/>
	<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url();?>img/favicon/favicon-32x32.png"/>
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>img/favicon/favicon-16x16.png"/>
	<link rel="mask-icon" href="<?=base_url();?>img/favicon/safari-pinned-tab.svg" color="#177dff"/>
	<meta name="theme-color" content="#177dff"/>
	<meta name="msapplication-TileColor" content="#177dff"/>
	<meta name="msapplication-navbutton-color" content="#177dff"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="#177dff"/>
	<title>CRM Maestros</title>
	<link href="<?=base_url();?>css/style.css" rel="stylesheet"></head>
<body>
<div id="app"></div>
<script type="text/javascript" src="<?=base_url();?>js/script.js"></script></body>
</html>