<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['migration_none_found']			= "Миграции не найдены.";
$lang['migration_not_found']			= "Эта миграция не найдена с версией: %s.";
$lang['migration_sequence_gap'] 		= "Разрыв в последовательности миграции в версии: %s.";
$lang['migration_multiple_version']		= "Есть несколько миграций с указанным номером версии: %s.";
$lang['migration_class_doesnt_exist']	= "Класс миграций \"%s\" не найден.";
$lang['migration_missing_up_method']	= "Класс миграций \"%s\" не содержит метод 'up'.";
$lang['migration_missing_down_method']	= "Класс миграций \"%s\" не содержит метод 'down'.";
$lang['migration_invalid_filename']		= "Миграция \"%s\" содержит неправильное имя файла.";