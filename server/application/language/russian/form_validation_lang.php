<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']				= 'Требуется указать {field}.';
$lang['form_validation_isset']					= 'Поле {field} должно содержать значение.';
$lang['form_validation_valid_email']			= 'Поле {field} должно содержать правильный адрес электронной почты.';
$lang['form_validation_valid_emails']			= 'Все адреса электронной почты в поле {field} должны быть правильными.';
$lang['form_validation_valid_url']				= 'Поле {field} должно содержать корректный URL.';
$lang['form_validation_valid_ip']				= 'Поле {field} должно содержать правильный IP.';
$lang['form_validation_min_length']				= 'Поле {field} должно быть не короче {param} симв.';
$lang['form_validation_max_length']				= 'Поле {field} должно быть не длиннее {param} симв.';
$lang['form_validation_exact_length']			= 'Поле {field} должно быть длинной точно {param} симв.';
$lang['form_validation_alpha']					= 'Поле {field} может содержать только алфавитные символы.';
$lang['form_validation_alpha_numeric']			= 'Поле {field} может содержать только алфавитно-числовые символы.';
$lang['form_validation_alpha_numeric_spaces']	= 'Поле {field} может содержать только алфавитно-числовые символы и пробелы.';
$lang['form_validation_alpha_dash']				= 'Поле {field} может содержать только алфавитно-числовые символы, знак подчеркивания и дефис.';
$lang['form_validation_numeric']				= 'Поле {field} может содержать только числа.';
$lang['form_validation_is_numeric']				= 'Поле {field} может содержать только числовые символы.';
$lang['form_validation_integer']				= 'Поле {field} может содержать только целое число.';
$lang['form_validation_regex_match']			= 'Поле {field} некорректного формата.';
$lang['form_validation_matches']				= 'Поле {field} должно совпадать с полем {param}.';
$lang['form_validation_differs']				= 'Поле {field} должно отличаться от поля {param}.';
$lang['form_validation_is_unique'] 				= 'Поле {field} должно содержать уникальное значение.';
$lang['form_validation_is_natural']				= 'Поле {field} должно содержать только положительные числа.';
$lang['form_validation_is_natural_no_zero']		= 'Поле {field} должно содержать число больше ноля.';
$lang['form_validation_decimal']				= 'Поле {field} должно содержать десятичное число.';
$lang['form_validation_less_than']				= 'Поле {field} должно содержать число меньше {param}.';
$lang['form_validation_less_than_equal_to']		= 'Поле {field} должно содержать число меньше или равно {param}.';
$lang['form_validation_greater_than']			= 'Поле {field} должно содержать число больше {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Поле {field} должно содержать число больше или равно {param}.';
$lang['form_validation_error_message_not_set']	= 'Сообщение ошибки не установлено для поля {field}.';
$lang['form_validation_in_list']				= 'Поле {field} должно быть одним из: {param}.';