<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['terabyte_abbr'] = "ТБ";
$lang['gigabyte_abbr'] = "ГБ";
$lang['megabyte_abbr'] = "МБ";
$lang['kilobyte_abbr'] = "КБ";
$lang['bytes'] = "Байт";